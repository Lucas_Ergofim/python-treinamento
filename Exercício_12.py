'''Faça um script Python para conversão de segundos. O usuário irá informar os segundos
como um número inteiro. O script deve calcular quantas horas:minutos:segundos a
quantidade total de segundos digitados pelo usuário corresponde.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                            '+ color.UNDERLINE + color.DARKCYAN +  'CONVERSÃO DE SEGUNDO PARA HORAS/MINUTOS/SEGUNDOS'+ color.END)

second = int(input('Informe aqui os segundos: '))
secondToHour = (second/3600) # Segundos para horas, sem levar em conta inteiro e resto.
secondToMinute = second/60
hour = int(second/3600)
day = (hour/24)
week = (day / 7)
month = (week / 4.205)
year =(month/11.613)

minute = int(secondToMinute%60)
sec = second%60
mil = second*1000

print(color.DARKCYAN + f'{second} segundo(s)' + color.END)
print(color.YELLOW + f'{secondToHour} hora(s)' + color.END)
print(color.BLUE + f'{secondToMinute} minuto(s)' + color.END)
print(color.RED + f'{mil} milissegundo(s)' + color.END)
print(color.GREEN + f'{day} dia(s)' + color.END)
print(color.YELLOW + f'{week} semana(s)' + color.END)
print(color.DARKCYAN + f'{month} mês(meses)' + color.END)


print(f'\nO horário convertido é:' + color.PURPLE + f'\n{hour}:{minute}:{sec}' + color.END )

if hour>24:
   day = int(hour / 24)
   hour = int(second / 3600 % 24)
   print(f'\nOs segundos convertidos em dia(s) e relógio são:' + color.PURPLE + f'\n{day} dia(s) {hour}:{minute}:{sec}' + color.END)

if day>=7 and day <30:
   week = int(day/7)
   day = int(day%7)
   print(f'\nOs segundos convertidos em semana(s), dia(s) e relógio são:' + color.PURPLE + f'\n{week} semana(s) {day} dia(s) {hour}:{minute}:{sec}' )

if day>=30 and day<30:
   month = int(day/30)
   week = int(day%30/7)
   day = int(day%30%7)
   print(f'\nOs segundos convertidos em mês(meses) semana(s), dia(s) e relógio são:' + color.PURPLE + f'\n{month} mês(meses) {week} semana(s) {day} dia(s) {hour}:{minute}:{sec}' )

if day>=365:
   year = int(day/365)
   month = int(day%365/30)
   week = int(day%365%30/7)
   day = int(day%365%30%7)
   print(f'\nOs segundos convertidos em mês(meses) semana(s), dia(s) e relógio são:' + color.PURPLE + f'\n{year} ano(s), {month} mês(meses), {week} semana(s), {day} dia(s) e {hour}:{minute}:{sec}' )
