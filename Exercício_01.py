'''Faça um script Python para exibir a multiplicação de dois números inteiros informados pelo
usuário.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'MULTIPLICAÇÃO'+ color.END)
n1 = int(input('Insira o primeiro fator: '))
n2 = int(input('Insira o primeiro fator: '))
multi = n1*n2
print(f'O resultado da multiplicação é {multi}')

