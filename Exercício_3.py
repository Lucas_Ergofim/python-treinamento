'''3) Faça um script Python para ler 3 números reais e exibir a soma do 1o número com o 2o
multiplicada pela soma do 2o com o 3o.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'SOMA E MULTIPLICAÇÃO'+ color.END)
num, num1, num2 = float(input('Insira um número: ')), float(input('Insira um número: ')), float(input('Insira um número: '))
result = (num+num1)*(num1+num2)

print(f'a soma do {num} com {num1} multiplicada pela soma de {num1} com {num2} é: {result}')
