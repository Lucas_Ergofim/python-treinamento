'''Escreva um script Python para ler três números reais e exibir o triplo de sua soma.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'TRIPLO DA SOMA'+ color.END)
num = float(input('Digite o primeiro número: '))
num1 = float(input('Digite o segundo número: '))
num2 = float(input('Digite o terceiro número: '))

triDeSoma = ((num+num1+num2)*3)
print(f'O triplo da soma de {num}, {num1} e {num2} é igual a ' + color.BOLD + color.GREEN + f'{triDeSoma}')