'''Escreva um script Python para ler um número real e imprimir o seu cubo.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'CUBO'+ color.END)
num = float(input('Digite um número, para descobrir o seu cubo: '))
num = (num**3)
print(num)