'''Crie um script Python que calcule a área de um círculo pela fórmula  Area = π*r²
para o usuário informar o valor do raio (r). Use variáveis do tipo real (float).
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'ÁREA DO CÍRCULO'+ color.END)
pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461
r = float(input("Digite aqui o valor do raio: "))
area = pi*(r ** 2)
print(f'O valor da área: {area}')
