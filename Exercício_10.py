'''Escreva um script Python para ler o salário de um funcionário e imprimi-lo com aumento
de 15%.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'AUMENTO SALARIAL'+ color.END)

wage = float(input(color.RED + 'Digite o seu salário R$ ' + color.END))
increase = wage*15/100
finalWage = increase + wage
print(f'O salário do funcionário com o aumento de 15% é igual: R$ ' + color.GREEN + f'{finalWage}')