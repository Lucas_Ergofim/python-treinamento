'''Escreva um script Python ler dois inteiros e exibir o resto da divisão do primeiro pelo
segundo. Se o usuário quiser dividir um número por zero, faça a verificação de divisão por zero
e informe que não é possível esta operação.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'RESTO'+ color.END)
num = int(input('Digite o dividendo: '))
num1 = int(input('Digite o divisor: '))
if num1 != 0:
      resto = num%num1
      print(f'O resto da divisão é: {resto}')
else:
      print('A operação não é possível!!')