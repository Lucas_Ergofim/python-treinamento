'''Faça um script Python para ler um número inteiro e exibir o seu dobro.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'DOBRO'+ color.END)
num = int(input('Insira um número: '))
dobro = num*2


print(f'O dobro de {num} é {dobro}')

