'''Considere que uma pessoa investe inicialmente R$1 500,00 em uma conta poupança
rendendo 8% de juros ao ano. Se esta pessoa não retirar nenhum valor da conta e ainda
adiciona mais R$ 557,00 a cada ano, quanto em R$ haverá nesta conta ao fim de 25 anos? Faça
um script Python que mostre este resultado, pedindo para o usuário digitar o valor de
investimento inicial e anual.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'INVESTIMENTO'+ color.END)
capital_inicial = float(input('Digite o depósito inicial: R$'))
i = float(input('Agora, digite a taxa de juros: '))
i = (i/100) # Definindo o valor de i separadamente para deixar o código mais limpo
t = 1   # Contador dos anos
aporte = 0 # Não coloque c2. É muito confuso.

while t < 25:
    if t == 1:
        m = capital_inicial + (capital_inicial * i) # Ou: a = capital_inicial * (1 + i)
        print(f'\nNo ano {t}, o valor dos juros será igual a R${m:.2f}')
    else:
        print(f'\nNo ano {t}, o valor dos juros será igual a R${m:.2f}')

    t = t + 1 # Mesma coisa escrever: t += 1

    op = input(f'Deseja realizar um depósito para o {t}° ano[S/N]? ').strip().upper()[0]
    while op != 'S' and op != 'N':
        op = input(f'OPÇÃO INVÁLIDA! Deseja realizar um depósito para o {t}° ano[S/N]? ').strip().upper()[0]

    if op == 'S':
        aporte = float(input(f'Digite o valor do depósito do {t}° ano: R$'))
    else:
        aporte = 0

    m = m + (m * i) + aporte # Ou: m = m * (1 + i) + aporte

ganho_com_juros = m - capital_inicial # (montante final - capital inicial)
print(f'No total, a quantidade de reais ganha com os JUROS COMPOSTOS será igual a R${ganho_com_juros}')