'''Criem um programa que calcule o valor de delta, usando a fórmula DELTA = b² - 4ac
Peça os valores para o usuário. Use variáveis do tipo real (float).
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                             '+ color.UNDERLINE + color.DARKCYAN +  'VALOR DE DELTA'+ color.END)

a = float(input('Insira o valor de a: '))
b = float(input('Insira o valor de b: '))
c = float(input('Insira o valor de c: '))
delta = (b**2) - 4*a*c

print(f'\nO valor de delta é: {delta}')

