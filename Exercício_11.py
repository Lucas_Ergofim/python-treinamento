'''Escreva um script Python para ler dois valores reais e exibir o primeiro com acréscimo de
30% e o segundo com desconto de 25%.
'''
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

print('                            '+ color.UNDERLINE + color.DARKCYAN +  'ACRÉSCIMOS'+ color.END)

num = float(input('Digite o primeiro número: '))
increase = (num*30/100)
totalIncrease = num + increase
print(f'O número '+ color.RED + f'{num} '+ color.END + 'com acréscimo de 30% é igual a ' + color.GREEN +f'{totalIncrease}'+ color.END)

num1 = float(input('Digite o segundo número: '))
increase1 = (num1*25/100)
totalIncrease1 = num1 + increase1
print(f'O número '+ color.RED + f'{num1} '+ color.END + 'com acréscimo de 25% é igual a ' + color.GREEN +f'{totalIncrease1}'+ color.END)
